module javafx {
    requires javafx.controls;
    requires javafx.media;

    opens cz.cvut.fit.miadp;
    exports cz.cvut.fit.miadp;
}