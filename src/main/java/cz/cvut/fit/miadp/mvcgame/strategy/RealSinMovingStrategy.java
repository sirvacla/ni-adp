package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;

 public class RealSinMovingStrategy implements IMovingStrategy {
    @Override
    public void updatePosition( AbsMissile missile ) {
        double initAngle = missile.getInitAngle( );
        int initVelocity = missile.getInitVelocity( );
        long time = missile.getAge( ) / 100;

        int dX = ( int )( initVelocity * time/20 );
        int dY = ( int )( initVelocity * Math.cos( time ) );

        int[] vector = new int[] { dX, dY };
        missile.move( vector );

    }
}
