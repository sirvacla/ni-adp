package cz.cvut.fit.miadp.mvcgame.state;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;

public class DynamicShootingMode implements IShootingMode {
    @Override
    public void shoot( AbsCannon cannon ) {
        int half = cannon.getNumberOfMissiles()/2;
        int middleShot = cannon.getNumberOfMissiles()%2;

        for(int i = half; i>0; i--){
            cannon.aimUp();
            cannon.primitiveShoot();
        }

        for(int i = half; i>0; i--){
            cannon.aimDown();
        }

        if(middleShot == 1)
            cannon.primitiveShoot();

        for(int i = half; i>0; i--){
            cannon.aimDown();
            cannon.primitiveShoot();
        }

        for(int i = half; i>0; i--){
            cannon.aimUp();
        }
    }

    @Override
    public String getName( ) {
        return "DoubleShootingMode";
    }
}
