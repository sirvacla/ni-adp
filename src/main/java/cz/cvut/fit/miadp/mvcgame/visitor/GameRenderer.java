package cz.cvut.fit.miadp.mvcgame.visitor;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;


public class GameRenderer implements IVisitor {

    private IGameGraphics gr;

    public void setGraphicContext( IGameGraphics gr ) {
        this.gr = gr;
    }

    @Override
    public void visitCannon(AbsCannon cannon) {
        this.gr.drawImage( "cannon.png", cannon.getPosition( ) );
    }

    @Override
    public void visitMissile(AbsMissile missile) {
        this.gr.drawImage( "missile.png", missile.getPosition( ) );
    }
    
}
