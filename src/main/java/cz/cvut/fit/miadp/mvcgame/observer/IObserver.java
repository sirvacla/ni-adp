package cz.cvut.fit.miadp.mvcgame.observer;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;

public interface IObserver<T> {
    void update( T observable, MvcGameConfig.OBSERVER_ASPECTS aspect );

    void update( );
    
}
