package cz.cvut.fit.miadp.mvcgame.model;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;

import java.util.Arrays;

public class Position {

	private int[] position = new int[MvcGameConfig.NUMBER_OF_DIMENSIONS];

	public Position(){}

	public Position( int[] position){
		this.position = position;
	}

	public void setPosition(int[] position) {
		this.position = position;
	}

	public int[] getPosition() {
		return position.clone();
	}

	public void add( int[] movePosition ) {
		for (int index = 0; index < movePosition.length; index++){
			position[index] += movePosition[index];
		}
	}
	
}