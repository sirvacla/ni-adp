package cz.cvut.fit.miadp.mvcgame.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.LinkedBlockingQueue;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.GameObjectFactory_A;
import cz.cvut.fit.miadp.mvcgame.abstractFactory.IGameObjectFactory;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;
import cz.cvut.fit.miadp.mvcgame.command.AbstractGameCommand;
import cz.cvut.fit.miadp.mvcgame.observer.IObserver;
import cz.cvut.fit.miadp.mvcgame.strategy.*;
import cz.cvut.fit.miadp.mvcgame.visitor.GameSoundEffector;


public class GameModel implements IGameModel {

    private AbsCannon cannon;
    private List<AbsMissile> missiles;
    private List<IObserver<GameModel>> observers;
    private IGameObjectFactory goFact;

    private GameSoundEffector soundPlayer;
    private IMovingStrategy movingStrategy;

    private Queue<AbstractGameCommand> unExecutedCmds;
    private Stack<AbstractGameCommand> executedCmds;

    private int score;


    public GameModel( ) {
        this.observers = new ArrayList<>();
        this.goFact = new GameObjectFactory_A( this );
        this.cannon = this.goFact.createCannon( );
        this.soundPlayer = new GameSoundEffector();
        this.missiles = new ArrayList<>();
        this.movingStrategy = new SimpleMovingStrategy( );
        this.score = 0;
        this.unExecutedCmds = new LinkedBlockingQueue<>();
        this.executedCmds = new Stack<>();

        this.cannon.acceptVisitor(soundPlayer);
    }

    public void update( ) {
        this.executedCmds( );
        this.moveMissiles( );
    }

    private void executedCmds( ) {
        while( !this.unExecutedCmds.isEmpty( ) ){
            AbstractGameCommand cmd = this.unExecutedCmds.poll( );
            cmd.doExecute( );
            this.executedCmds.push( cmd );
        }
    }

    private void moveMissiles( ) {
        for ( AbsMissile missile : this.missiles ) {
            missile.move(  );
        }
        this.destroyMissiles( );
        this.notifyObservers( MvcGameConfig.OBSERVER_ASPECTS.RENDER );
    }

    private void destroyMissiles( ) {
        List<AbsMissile> missilesToRemove = new ArrayList<>();
        for ( AbsMissile missile : this.missiles ) {
            if( missile.getPosition( ).getPosition()[0] > MvcGameConfig.MAX_X ) {
                missilesToRemove.add( missile );
            }
        }
        this.missiles.removeAll(missilesToRemove);
    }

    public Position getCannonPosition( ) {
        return this.cannon.getPosition( );
    }

    public void moveCannonUp( ) {
        this.cannon.moveUp( );
        this.notifyObservers( MvcGameConfig.OBSERVER_ASPECTS.RENDER );
    }

    public void moveCannonDown( ) {
        this.cannon.moveDown( );
        this.notifyObservers( MvcGameConfig.OBSERVER_ASPECTS.RENDER );
    }

    @Override
    public void registerObserver( IObserver obs ) {
        if( !this.observers.contains( obs ) ) {
            this.observers.add( obs );
        }
    }

    @Override
    public void unregisterObserver( IObserver obs ) {
        if( this.observers.contains( obs ) ) {
            this.observers.remove( obs );
        }
    }

    @Override
    public void notifyObservers( MvcGameConfig.OBSERVER_ASPECTS aspect ) {
        for( IObserver obs : this.observers ){
            obs.update(this, aspect );
        }
    }

    @Override
    public void notifyObservers(){
        for( IObserver obs : this.observers ){
            obs.update( );
        }
    }

    public void cannonShoot( ) {
        this.missiles.addAll( cannon.shoot() );
        this.notifyObservers( MvcGameConfig.OBSERVER_ASPECTS.RENDER );
    }

    @Override
    public List<AbsMissile> getMissiles() {
        return null;
    }

    public List<GameObject> getGameObjects( ) {
        List<GameObject> go = new ArrayList<>();
        go.add( this.cannon );
        go.addAll( this.missiles );
        return go;
    }

    public IMovingStrategy getMovingStrategy( ){
        return this.movingStrategy;
    }

    public void toggleMovingStrategy( ) {
        if ( this.movingStrategy instanceof SimpleMovingStrategy ) {
            this.movingStrategy = new RealisticMovingStrategy( );
        }
        else if ( this.movingStrategy instanceof RealisticMovingStrategy ){
            this.movingStrategy = new SinMovingStrategy( );
        }
        else if ( this.movingStrategy instanceof SinMovingStrategy){
            this.movingStrategy = new RealSinMovingStrategy( );
        }
        else if ( this.movingStrategy instanceof RealSinMovingStrategy){
            this.movingStrategy = new SimpleMovingStrategy( );
        }
        else {}
    }

    public void toggleShootingMode( ){
        this.cannon.toggleShootingMode( );
    }

    private class Memento {
        private int score;
        private int cannonX;
        private int cannonY;
    }

    public Object createMemento( ) {
        Memento m = new Memento( );
        m.score = this.score;
        m.cannonX = this.getCannonPosition( ).getPosition()[0];
        m.cannonY = this.getCannonPosition( ).getPosition()[1];
        return m;
    }

    public void setMemento( Object memento ) {
        Memento m = ( Memento ) memento;
        this.score = m.score;
        this.cannon.getPosition( ).setPosition(new int[]{m.cannonX, m.cannonY});
    }

    @Override
    public void registerCommand( AbstractGameCommand cmd ) {
        this.unExecutedCmds.add( cmd );
    }

    @Override
    public void undoLastCommand( ) {
        if( !this.executedCmds.isEmpty( ) ){
            AbstractGameCommand cmd = this.executedCmds.pop( );
            cmd.unExecute( );
        }
        this.notifyObservers( );
    }

    public void aimCannonUp( ) {
        this.cannon.aimUp( );
        this.notifyObservers( );
    }

    public void aimCannonDown( ) {
        this.cannon.aimDown( );
        this.notifyObservers( );
    }

    public void cannonPowerUp( ) {
        this.cannon.powerUp( );
        this.notifyObservers( );
    }

    public void cannonPowerDown( ) {
        this.cannon.powerDown( );
        this.notifyObservers( );
    }

    public void cannonMissilesUp(){
        this.cannon.missilesUp();
        this.notifyObservers();
    }

    public void cannonMissilesDown(){
        this.cannon.missilesDown();
        this.notifyObservers();
    }
}