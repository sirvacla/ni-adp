package cz.cvut.fit.miadp.mvcgame.abstractFactory;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_A.Cannon_A;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_A.Missile_A;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;

public class GameObjectFactory_A implements IGameObjectFactory {

    private IGameModel model;

    public GameObjectFactory_A(IGameModel model) {
        this.model = model;
    }


    @Override
    public Cannon_A createCannon() {
        return new Cannon_A(new Position(MvcGameConfig.CANNON_POSITION), this);
    }

    @Override
    public Missile_A createMissile(double initAngle, int initVelocity) {
        return new Missile_A(
                new Position(
                        model.getCannonPosition().getPosition()
                ),
                initAngle,
                initVelocity,
                this.model.getMovingStrategy()
        );
    }
}
