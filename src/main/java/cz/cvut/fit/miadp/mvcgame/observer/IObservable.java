package cz.cvut.fit.miadp.mvcgame.observer;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;

public interface IObservable {

    void registerObserver( IObserver obs );
    void unregisterObserver( IObserver obs );
    void notifyObservers( MvcGameConfig.OBSERVER_ASPECTS aspect );

    void notifyObservers( );
}
