package cz.cvut.fit.miadp.mvcgame.model;

import java.util.List;

import cz.cvut.fit.miadp.mvcgame.command.AbstractGameCommand;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;
import cz.cvut.fit.miadp.mvcgame.observer.IObservable;
import cz.cvut.fit.miadp.mvcgame.strategy.IMovingStrategy;

public interface IGameModel extends IObservable {
    void update( );
    Position getCannonPosition( );
    void moveCannonUp( );
    void moveCannonDown( );
    void aimCannonUp( );
    void aimCannonDown( );
    void cannonPowerUp( );
    void cannonPowerDown( );
    void cannonShoot( );
    List<AbsMissile> getMissiles( );
    List<GameObject> getGameObjects( );
    IMovingStrategy getMovingStrategy( );
    void toggleMovingStrategy( );
    void toggleShootingMode( );
    Object createMemento( );
    void setMemento( Object memento );
    void registerCommand( AbstractGameCommand cmd );
    void undoLastCommand( );
    void cannonMissilesUp();
    void cannonMissilesDown();
}
