package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;

public class SinMovingStrategy implements IMovingStrategy {

    @Override
    public void updatePosition( AbsMissile missile ) {
        double initAngle = missile.getInitAngle( );
        int initVelocity = missile.getInitVelocity( );
        long time = missile.getAge( ) / 100;

        int dX = ( int )( initVelocity * Math.cos( time ) * time/10 );
        int dY = ( int )( initVelocity * Math.sin( time ) * time/10 );

        int[] vector = new int[] { dX, dY };
        missile.move( vector );

    }
}
