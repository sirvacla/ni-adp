package cz.cvut.fit.miadp.mvcgame.bridge;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class JavaFxGraphics implements IGameGraphicsImplementor {

    private GraphicsContext gr;

    public JavaFxGraphics( GraphicsContext gr ) {
        this.gr = gr;
    }

    @Override
    public void drawImage(String path, Position pos) {
        Image image = new Image( path );
        this.gr.drawImage( image, pos.getPosition()[0], pos.getPosition()[1] );
    }

    @Override
    public void drawText(String text, Position pos) {
        this.gr.fillText(text, pos.getPosition()[0], pos.getPosition()[1] );
    }

    @Override
    public void drawLine(Position beginPosition, Position endPosition) {
        this.gr.strokeLine( beginPosition.getPosition()[0],  beginPosition.getPosition()[1], endPosition.getPosition()[0],  endPosition.getPosition()[1]);
    }

    @Override
    public void clear( ) {
        this.gr.clearRect( 0, 0, MvcGameConfig.MAX_X,  MvcGameConfig.MAX_Y );
    }

}
