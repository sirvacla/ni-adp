//package cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_B;
//
//import cz.cvut.fit.miadp.mvcgame.model.Position;
//import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
//
//import java.util.Random;
//
//public class Missile_B extends AbsMissile {
//
//    final Random randomNumberGenerator;
//
//    public Missile_B( Position initialPosition ){
//        this.randomNumberGenerator = new Random();
//        this.position = initialPosition;
//    }
//
//    @Override
//    public void move(int[] vector){
//        vector[0] *= randomNumberGenerator.nextInt(10);
//        this.position.add(vector);
//    }
//}
