//package cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_B;
//
//import cz.cvut.fit.miadp.mvcgame.abstractFactory.IGameObjectFactory;
//import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
//import cz.cvut.fit.miadp.mvcgame.model.Position;
//import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;
//import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
//
//import java.util.Arrays;
//import java.util.Random;
//
//public class Cannon_B extends AbsCannon {
//
//    private IGameObjectFactory goFact;
//    private Random randomNumberGenerator;
//
//    public Cannon_B(Position initialPosition, IGameObjectFactory goFact ){
//        this.position = initialPosition;
//        this.goFact = goFact;
//        randomNumberGenerator = new Random();
//    }
//
//    @Override
//    public void moveUp() {
//        int[] vector = new int[MvcGameConfig.NUMBER_OF_DIMENSIONS];
//        Arrays.fill(vector, 0);
//        vector[1] = -1 * randomNumberGenerator.nextInt(10) * MvcGameConfig.MOVE_STEP;
//        this.move( vector );
//    }
//
//    @Override
//    public void moveDown() {
//        int[] vector = new int[MvcGameConfig.NUMBER_OF_DIMENSIONS];
//        Arrays.fill(vector, 0);
//        vector[1] = randomNumberGenerator.nextInt(10) * MvcGameConfig.MOVE_STEP;
//        this.move( vector );
//    }
//
//    @Override
//    public AbsMissile shoot() {
//        return this.goFact.createMissile( new Position( this.getPosition().getPosition() ) );
//    }
//}
