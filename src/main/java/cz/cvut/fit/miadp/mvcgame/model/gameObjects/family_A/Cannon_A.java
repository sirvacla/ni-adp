package cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_A;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.IGameObjectFactory;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.Vector;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;


public class Cannon_A extends AbsCannon {

    private IGameObjectFactory goFact;

    private double angle;
    private int power;

    private int numberOfMissiles;
    private List<AbsMissile> shootingBatch;

    public Cannon_A( Position initialPosition, IGameObjectFactory goFact ){
        this.position = initialPosition;
        this.goFact = goFact;

        this.power = MvcGameConfig.INIT_POWER;
        this.angle = MvcGameConfig.INIT_ANGLE;
        this.numberOfMissiles = MvcGameConfig.INIT_NUMBER_OF_MISSILES;
        this.shootingMode = AbsCannon.SINGLE_SHOOTING_MODE;
        this.shootingBatch = new ArrayList<AbsMissile>();
    }

    public void moveUp( ) {
        int[] vector = new int[MvcGameConfig.NUMBER_OF_DIMENSIONS];
        Arrays.fill(vector, 0);
        vector[1] = -1*MvcGameConfig.MOVE_STEP;
        this.move( vector );
    }

    public void moveDown( ) {
        int[] vector = new int[MvcGameConfig.NUMBER_OF_DIMENSIONS];
        Arrays.fill(vector, 0);
        vector[1] = MvcGameConfig.MOVE_STEP;
        this.move( vector );
    }

    @Override
    public List<AbsMissile> shoot( ) {
        this.shootingBatch.clear( );
        this.shootingMode.shoot( this );
        return this.shootingBatch;
    }

    @Override
    public void aimUp() {
        this.angle -= MvcGameConfig.ANGLE_STEP;
    }

    @Override
    public void aimDown() {
        this.angle += MvcGameConfig.ANGLE_STEP;
    }

    @Override
    public void powerUp() {
        this.power += MvcGameConfig.POWER_STEP;
    }

    @Override
    public void powerDown() {
        if ( this.power - MvcGameConfig.POWER_STEP > 0 ){
            this.power -= MvcGameConfig.POWER_STEP;
        }
    }

    @Override
    public void missilesUp(){
        this.numberOfMissiles++;
    }

    @Override
    public void missilesDown(){
        if ( this.numberOfMissiles > MvcGameConfig.INIT_NUMBER_OF_MISSILES ){
            this.numberOfMissiles--;
        }
    }

    @Override
    public int getNumberOfMissiles(){
        return this.numberOfMissiles;
    }

    @Override
    public void primitiveShoot() {
        this.shootingBatch.add(
                this.goFact.createMissile(
                        this.angle,
                        this.power
                )
        );
    }
}
