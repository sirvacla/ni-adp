package cz.cvut.fit.miadp.mvcgame.bridge;

import cz.cvut.fit.miadp.mvcgame.model.Position;

public class GameGraphics implements IGameGraphics {

    IGameGraphicsImplementor implementor;

    public GameGraphics( IGameGraphicsImplementor implementor ){
        this.implementor = implementor;
    }

    @Override
    public void drawImage( String path, Position pos ) {
        this.implementor.drawImage( path, pos );
    }

    @Override
    public void drawText( String text, Position pos ) {
        this.implementor.drawText( text, pos );
    }

    @Override
    public void drawRectangle( Position leftTop, Position rightBottom ) {
        this.implementor.drawLine( leftTop, new Position( new int[] {rightBottom.getPosition()[0], leftTop.getPosition()[1]} ) );
        this.implementor.drawLine( new Position( new int[] {rightBottom.getPosition()[0], leftTop.getPosition()[1]} ), rightBottom );
        this.implementor.drawLine( rightBottom, new Position( new int[] {leftTop.getPosition()[0], rightBottom.getPosition()[1]} ) );
        this.implementor.drawLine( new Position( new int[] {leftTop.getPosition()[0], rightBottom.getPosition()[1]} ), leftTop );
    }

    @Override
    public void clear() {
        this.implementor.clear( );
    }

}