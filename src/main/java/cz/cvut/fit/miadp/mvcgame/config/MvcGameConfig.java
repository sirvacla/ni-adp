package cz.cvut.fit.miadp.mvcgame.config;

import java.util.ArrayList;
import java.util.List;

public class MvcGameConfig {
    public static final int MAX_X = 1280;
    public static final int MAX_Y = 720;
    public static final int MOVE_STEP = 10;

//  public static final int CANNON_POS_X = 50;
//  public static final int CANNON_POS_Y = MAX_Y / 2;

    public static final int POWER_STEP = 1;
    public static final int INIT_POWER = 10;
    public static final double INIT_ANGLE = 0;
    public static final double ANGLE_STEP = Math.PI / 18;
    public static final double GRAVITY = 9.8;

    public static final int INIT_NUMBER_OF_MISSILES = 3;

    public static final int NUMBER_OF_DIMENSIONS = 2;

    public static final int[] CANNON_POSITION = {10, MAX_Y/2};

  public enum OBSERVER_ASPECTS {
    RENDER
  }
}