package cz.cvut.fit.miadp.mvcgame.visitor;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import java.io.File;

public class GameSoundEffector implements IVisitor{
    Media missileSound = new Media(new File("Cannon_sound.wav").toURI().toString());
    Media cannonSound = new Media(new File("retro-city-14099.mp3").toURI().toString());
    MediaPlayer missileMediaPlayer = new MediaPlayer(missileSound);
    MediaPlayer cannonMediaPlayer = new MediaPlayer(cannonSound);

    @Override
    public void visitCannon(AbsCannon cannon) {
        this.cannonMediaPlayer.play();
    }

    @Override
    public void visitMissile(AbsMissile missile) {
        this.missileMediaPlayer.play();
    }
}
