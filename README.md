# mvcgame-ni-adp-2022

### .vscode/launch.json ###

```
{
    "configurations": [
        {
            "type": "java",
            "name": "CodeLens (Launch) - MvcGameJavaFxLauncher",
            "request": "launch",
            "mainClass": "cz.cvut.fit.miadp.MvcGameJavaFxLauncher",
            "vmArgs": "--module-path C:/Users/tomas/Documents/FIT/ADP2022/javafx-sdk-19/lib --add-modules javafx.controls",
            "projectName": "mvcgame"
        }
    ]
}
```

